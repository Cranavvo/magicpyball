'''
Created on 15 Feb 2014

@author: Connor
'''

import random as numbers

Answers = ["The odds are not in your favor.", "Ask again later when the stars are shining.", "Yes.", 
           "Possibly.", "It's likely.", "No.", "You may need to wait until the moon is out.",
           "The outcome doesn't look very good.", "The outcome looks brilliant!"]

AnswersCount = len(Answers)

Question = raw_input("What is your question: ")

print(Answers[numbers.randrange(0, AnswersCount)])

